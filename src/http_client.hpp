#ifndef HTTP_CLIENT_HPP
#define HTTP_CLIENT_HPP

#include <string>
#include <cstring>
#include <vector>
#include <algorithm>
#include <iostream>
#include <sstream>
#include <map>
#include <chrono>
#include <iomanip>

#include "openssl/ssl.h"
#include "openssl/bio.h"
#include "openssl/err.h"

#include "uri_encode.hpp"

class HTTPClient {
	using Dict = std::map<std::string, std::vector<std::string> >;
	using Cookie = struct {
		std::string value;
		
	};

	using Duration = std::chrono::duration<double>;
	using Clock = std::chrono::system_clock;
	using TimePoint = std::chrono::time_point<Clock>;

	private:
		SSL_CTX * ctx;
		std::map<std::string, Cookie> cookies;

		static constexpr const char* GET_METHOD = "GET";
		static constexpr const char* HTTP_VERSION_PREFIX = "HTTP/";
		static constexpr const char* HTTP_DEFAULT_VERSION = "1.1";
		static constexpr const char* SPACE = " ";
		static constexpr const char* LINE_END = "\r\n";
		static constexpr const char* DOUBLE_LINE_END = "\r\n\r\n";
		static constexpr const char* PROTOCOL_DELIMITER = "://";
		static constexpr const char* SLASH = "/";
		static constexpr const char* DOUBLE_DOT = ":";
		static constexpr const char* HTTP = "http";
		static constexpr const char* HTTPS = "https";
		static constexpr const char* DEFAULT_PORT_HTTP = "80";
		static constexpr const char* DEFAULT_PORT_HTTPS = "443";
		static constexpr const char* CONTENT_LENGTH_HEADER = "content-length";
		static constexpr const char* TRANSFER_ENCODING_HEADER = "transfer-encoding";
		static constexpr const char* TRANSFER_ENCODING_CHUNKED = "chunked";
		static constexpr const char* LOCATION_HEADER = "location";
		static constexpr const std::size_t MAX_ADDR_LENGTH = 256;
		static constexpr const std::size_t BUFF_SIZE = 4096;

		static std::string trim(const std::string &str) {
			size_t first = str.find_first_not_of(' ');
		    if (std::string::npos == first)
		    {
		        return str;
		    }
		    size_t last = str.find_last_not_of(' ');
		    return str.substr(first, (last - first + 1));
		}

		static std::string toLower(const std::string &str) {
			std::string result(str.length(), '\0');
			std::transform(str.begin(), str.end(), result.begin(), ::tolower);
			return result;
		}

		static std::string toUpper(const std::string &str) {
			std::string result(str.length(), '\0');
			std::transform(str.begin(), str.end(), result.begin(), ::toupper);
			return result;
		}

		static std::vector<std::string> explode(const std::string& s, const std::string& delim, bool firstOnly = false)
		{
			std::string buff = s;
			std::vector<std::string> v;

			std::size_t pos = buff.find(delim);

			while (pos != std::string::npos) {
				std::string str = buff.substr(0, pos);
				//std::cout << "str : " << str << std::endl;
				v.push_back(str);
				buff = buff.substr(pos + delim.size());
				if (firstOnly) {
					break;
				}
				//std::cout << "buff : " << buff << std::endl;
				pos = buff.find(delim);
			}

			v.push_back(buff);

			return v;
		}

		static bool dictAddField(Dict &dict, const std::string &key, const std::string &value) {
			dict[key].push_back(value);
			return true;
		}

		static bool dictContainsKey(const Dict &dict, const std::string &key) {
			Dict::const_iterator pos = dict.find(key);
			return pos != dict.end();
		}

		static std::string readLine(BIO *bio) {
			std::string result;
			char buff[2];

			while (true) {
				std::size_t n = BIO_read(bio, buff, 1);
				buff[n] = '\0';
				result += buff;

				int lineSize = result.size();
				if (lineSize >= 2) {
					if (result[lineSize-2] == '\r' && result[lineSize-1] == '\n') {
						return result.substr(0,lineSize-2);
					}
				}
			}
		}

		static void parseURL(const std::string &url, std::string &protocol, std::string &host, std::string &port, std::string &resource) {
			std::string tmp(url);

			std::size_t pos = tmp.find(PROTOCOL_DELIMITER);
			
			if (pos != std::string::npos) {
				protocol = toLower(url.substr(0, pos));
				tmp = tmp.substr(pos+std::strlen(PROTOCOL_DELIMITER));
			} else {
				protocol = HTTP;
			}

			pos = tmp.find(SLASH);

			if (pos != std::string::npos) {
				host = tmp.substr(0, pos);
				resource = tmp.substr(pos);
			} else {
				host = tmp;
				resource = SLASH;
			}

			if (protocol.compare(HTTP) == 0) {
				port = DEFAULT_PORT_HTTP;
			} else if (protocol.compare(HTTPS) == 0) {
				port = DEFAULT_PORT_HTTPS;
			}

			std::vector<std::string> parts = explode(host, DOUBLE_DOT);

			if (parts.size() == 2) {
				host = parts[0];
				port = parts[1];
			}
		}

		static std::string buildRequest(
			const std::string &method, const std::string &resource,
			const Dict &params,
			const std::string &httpVersion = HTTP_DEFAULT_VERSION
		)  {
			std::stringstream request;

			request << method << SPACE << resource << SPACE << HTTP_VERSION_PREFIX << httpVersion << LINE_END;

			for (const auto &pair : params) {
				for (const auto &value : pair.second) {
					request << pair.first << DOUBLE_DOT << SPACE << value << LINE_END;
				}
			}

			request << LINE_END;

			return request.str();
		}

		static int parseHeader(
			std::vector<char> &responseStart,
			Dict &params
		) {
			std::string status, httpVersion;
			return parseHeader(responseStart, params, status, httpVersion);
		}

		static int parseHeader(
			std::vector<char> &responseStart,
			Dict &params,
			std::string &status,
			std::string &httpVersion
		) {
			int responseCode = -1;
			std::size_t pos;
			std::string responseText(responseStart.data());
			pos = responseText.find(DOUBLE_LINE_END);

			if (pos == std::string::npos) {
				throw std::runtime_error("Invalid response : header is not delimited.");
			}

			std::cout << "Header size : " << (pos + std::strlen(DOUBLE_LINE_END)) << std::endl;
			std::cout << "--Header received--" << std::endl;
			std::cout << responseText.substr(0, pos) << std::endl;
			std::cout << "-------------------" << std::endl;
			std::cout << "Parsing..." << std::endl;

			std::vector<std::string> fields = explode(responseText.substr(0, pos), LINE_END);
			responseStart.erase(responseStart.begin(), responseStart.begin() + pos+std::strlen(DOUBLE_LINE_END));

			for (const std::string &field : fields) {
				std::vector<std::string> parts = explode(field, DOUBLE_DOT, true);

				if (parts.size() == 1) {
					parts = explode(parts[0], SPACE);
					std::size_t pos2 = toUpper(parts[0]).find(HTTP_VERSION_PREFIX);

					if (pos2 != std::string::npos) {
						if (parts.size() > 1) {
							try {
								responseCode = std::stoi(parts[1]);
							} catch (const std::exception &e) {
								throw std::runtime_error("Invalid response : cannot parse response code.");
							}
						}

						if (parts.size() > 2) {
							status = parts[2];
						}

						parts = explode(parts[0], SLASH);

						if (parts.size() == 2) {
							httpVersion = parts[1];
						}
					}
				} else if (parts.size() == 2) {
					params[toLower(trim(parts[0]))].push_back(trim(parts[1]));
				}
			}

			if (responseCode == -1) {
				throw std::runtime_error("Invalid response : cannot find response code.");
			}

			return responseCode;
		}

		static void recvResponseChunked(
			BIO *bio,
			std::vector<char> &responseStart,
			const Dict &headerParams,
			std::vector<char> &response
		) {
			int chunkSize;
			size_t n;
			std::vector<char> buff(BUFF_SIZE);
			std::size_t pos;

			std::cout << "Receiving response in chunked mode..." <<std::endl;

			do  {
				std::string responseText(responseStart.data());
				pos = responseText.find(LINE_END);

				if (pos == std::string::npos) {
					throw std::runtime_error("Invalid response format : cannot find chunk size.");
				}

				try {
					chunkSize = std::stoi(responseText.substr(0, pos), 0, 16);

					std::cout << "Current chunk size : " << chunkSize << std::endl;

					responseStart.erase(responseStart.begin(), responseStart.begin() + pos + strlen(LINE_END));
					n = std::min(chunkSize, (int)responseStart.size());
					response.insert(response.end(), responseStart.begin(), responseStart.begin() + n);
					chunkSize -= n;
					responseStart.erase(responseStart.begin(), responseStart.begin() + n);
				} catch (const std::exception &e) {
					throw std::runtime_error("Invalid response format : cannot parse chunk size.");
				}
			} while (responseStart.size() > 0);
			
			while (chunkSize != 0) {
				while (chunkSize > 0) {
					n = BIO_read(bio, buff.data(), std::min((int)BUFF_SIZE, chunkSize));

					response.insert(response.end(), buff.begin(), buff.begin() + n);
					chunkSize -= n;
				}

				readLine(bio);
				std::string nextChunkSize = readLine(bio);

				try {
					std::cout << "nextChunkSize : " << nextChunkSize << std::endl << "---" <<std::endl;
					chunkSize = std::stoi(nextChunkSize, 0, 16);
					std::cout << "Current chunk size : " << chunkSize << std::endl;
				} catch (const std::exception &e) {
					throw std::runtime_error("Invalid response format : cannot parse chunk size.");
				}
			}

			std::cout << "done." << std::endl;
		}

		static void recvResponse(
			BIO *bio,
			std::vector<char> &responseStart,
			const Dict &headerParams,
			std::vector<char> &response
		) {
			if (dictContainsKey(headerParams, TRANSFER_ENCODING_HEADER)) {
				if (toLower(trim(headerParams.at(TRANSFER_ENCODING_HEADER)[0])).compare(TRANSFER_ENCODING_CHUNKED) == 0) {
					recvResponseChunked(bio, responseStart, headerParams, response);
				} else {
					throw std::runtime_error("Unsupported transfer encoding.");
				}
			} else if (dictContainsKey(headerParams, CONTENT_LENGTH_HEADER)) {
				try {
					TimePoint lastPrint = Clock::now();

					std::size_t n;
					std::vector<char> buff(BUFF_SIZE);
					int contentLength = stoi(headerParams.at(CONTENT_LENGTH_HEADER)[0]);
					int restingSize = contentLength;
					response.insert(response.end(), responseStart.begin(), responseStart.end());
					restingSize -= responseStart.size();

					int currentRestingSize = restingSize;

					while (restingSize > 0) {
						//std::cout << "Waiting for " << std::min((int)BUFF_SIZE, restingSize) << " octet(s)" << std::endl;
						n = BIO_read(bio, buff.data(), std::min((int)BUFF_SIZE, restingSize));
						//std::cout << n << " octet(s) received" << std::endl;
						response.insert(response.end(), buff.begin(), buff.begin() + n);
						//std::cout << "Response size " << response.size() << std::endl;
						restingSize -= n;

						if (Duration(Clock::now() - lastPrint).count() > 1.0f) {
							float speed = (currentRestingSize - restingSize) / 1024.0f;
							float percent = ((float)(contentLength - restingSize)) / contentLength * 100.0f;
							std::cout << std::setprecision(2) << std::fixed << percent << "% received, ";
							std::cout << std::setprecision(0) << speed << " ko/s, ";
							std::cout << (contentLength - restingSize) << "/" << contentLength << " o" << std::endl;

							currentRestingSize = restingSize;
							lastPrint = Clock::now();
						}
					}

					std::cout << "100.00% received" << std::endl;

				} catch (const std::exception &e) {
					throw std::runtime_error("Cannot parse content length header.");
				}
			} else {
				throw std::runtime_error("Cannot find content length header.");
			}
		}

		void storeCookies(const std::vector<std::string> &setCookieHeaders) {
			for (const auto &setCookieHeader : setCookieHeaders) {
				std::vector<std::string> params = explode(setCookieHeader, ";");

				std::vector<std::string> parts = explode(params[0], "=");

				for (int i=2; i<parts.size(); i++) {
					parts[1] += "=";
					parts[1] += parts[i];
				}

				Cookie c;
				c.value = trim(parts[1]);
				cookies[trim(parts[0])] = c;
			}
		}

		std::string genCookieHeader() {
			std::string cookieHeader;
			for (const auto &cookie : cookies) {
				if (!cookieHeader.empty()) {
					cookieHeader += "; ";
				}

				cookieHeader += cookie.first;
				cookieHeader += "=";
				cookieHeader += cookie.second.value;
			}

			return cookieHeader;
		}

		BIO *createSecuredConnection(const std::string &host, const std::string &port) {
			std::string connectionString = host + ":" + port;

			BIO *bio;
			SSL * ssl;

			bio = BIO_new_ssl_connect(ctx);
			BIO_get_ssl(bio, & ssl);
			SSL_set_mode(ssl, SSL_MODE_AUTO_RETRY);

			BIO_set_conn_hostname(bio, connectionString.c_str());
 
			if(BIO_do_connect(bio) <= 0)
			{
			    throw std::runtime_error("Error connecting remote host.");
			}

			if(SSL_get_verify_result(ssl) != X509_V_OK)
			{
			    throw std::runtime_error("Cannot verify remote host certificate.");
			}

			return bio;
		}

		BIO *createUnsecuredConnection(const std::string &host, const std::string &port) {
			std::string connectionString = host + ":" + port;
			BIO *bio = BIO_new_connect(const_cast<char*>(connectionString.c_str()));
			
			if(bio == NULL)
			{
			    throw std::runtime_error("Error creating socket.");
			}
			 
			if(BIO_do_connect(bio) <= 0)
			{
			    throw std::runtime_error("Error connecting remote host.");
			}

			return bio;
		}

	public:
		HTTPClient() {
			//sockInit();
			SSL_load_error_strings();
			ERR_load_BIO_strings();
			OpenSSL_add_all_algorithms();
			OpenSSL_add_ssl_algorithms();

			ctx = SSL_CTX_new(SSLv23_client_method());

			if (ctx == NULL) {
				throw std::runtime_error("Error creating OpenSSL context.");
			}

			std::cout << "Loading trust store... ";

			if(! SSL_CTX_load_verify_locations(ctx, "../cacert.pem", NULL))
			{
			    throw std::runtime_error("Error loading trust store.");
			}

			std::cout << "done." << std::endl;
		}

		~HTTPClient() {
			SSL_CTX_free(ctx);
		}

		int get(const std::string &url, std::vector<char> &response/*, std::string &status*/, const Dict &requestParams = Dict()) {
			BIO *bio;
			std::string protocol, host, port, resource;

			parseURL(url, protocol, host, port, resource);

			
			std::cout << "url : " << url << std::endl;
			std::cout << "protocol : " << protocol << std::endl;
			std::cout << "host : " << host << std::endl;
			std::cout << "port : " << port << std::endl;
			std::cout << "resource : " << resource << std::endl;
			std::cout << std::endl;
			

			if (protocol.compare(HTTPS) == 0) {
				bio = createSecuredConnection(host, port);
			} else {
				bio = createUnsecuredConnection(host, port);
			}

			std::cout << "Connected to remote host" << std::endl;

			Dict params(requestParams);

			params["Host"].push_back(host);
			params["Accept-Encoding"].push_back("identity");
			params["Accept-Language"].push_back("en-US;q=0.8,en;q=0.2");
			//Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0
			params["User-Agent"].push_back("ZoroMiniHttp/1.0");
			
			if (!cookies.empty()) {
				params["Cookie"].push_back(genCookieHeader());
			}

			std::string request = buildRequest(GET_METHOD, resource, params);

			
			std::cout << "Sending request : " << std::endl;
			std::cout << "------------------" << std::endl;
			std::cout << request;
			std::cout << "------------------" << std::endl;
			

			BIO_write(bio, request.c_str(), request.size());

			int n;
			std::vector<char> responseStart(BUFF_SIZE);
			
			n = BIO_read(bio, responseStart.data(), BUFF_SIZE);
			responseStart.erase(responseStart.begin() + n, responseStart.end());

			std::cout << "Received " << n << " octets" << std::endl;
			std::cout << "response start size " << responseStart.size() << std::endl;

			std::string status, httpVersion;
			Dict headerParams;
			int responseCode = parseHeader(responseStart, headerParams, status, httpVersion);
			
			std::cout << "Response code : " << responseCode << std::endl;
			std::cout << "Status : " << status << std::endl;
			std::cout << "HTTP version : " << httpVersion << std::endl;
			std::cout << "--Headers params--" << std::endl;

			for (const auto &pair : headerParams) {
				for (const auto &value : pair.second) {
					std::cout << pair.first << " : " << value << std::endl;
				}
			}

			if (dictContainsKey(headerParams, "set-cookie")) {
				storeCookies(headerParams.at("set-cookie"));
			}

			std::cout << "--Stored cookies--" << std::endl;
			std::cout << genCookieHeader() << std::endl;
			std::cout << "------------------" << std::endl;

			std::cout << responseStart.size() << " octets of the entity was sent with the header." << std::endl;
			
			if (responseCode >= 300 && responseCode < 400) {
				std::cout << "Redirection to " << headerParams[LOCATION_HEADER][0] << std::endl;
				responseCode = get(headerParams[LOCATION_HEADER][0], response);
			} else {
				recvResponse(bio, responseStart, headerParams, response);
			}

			BIO_free_all(bio);

			return responseCode;
		}
};

#endif