#include "http_client.hpp"

#include <regex>
#include <fstream>

bool endsWith(const std::string& a, const std::string& b) {
    if (b.size() > a.size()) return false;
    return std::equal(a.begin() + a.size() - b.size(), a.end(), b.begin());
}

void test(const std::string &url) {
	std::cout << "----------------------------" << std::endl;
	try {

		
		std::vector<char> html;
		HTTPClient http;
		http.get(url, html);

		/*
		std::regex word_regex("(http|https)://([^/ :\"]+):?([^/ \"]*)(/?[^ #?\")]*)\\x3f?([^ #\")]*)([^ \")]*)");
	    auto words_begin = 
	        std::sregex_iterator(html.begin(), html.end(), word_regex);
	    auto words_end = std::sregex_iterator();

	    for (std::sregex_iterator i = words_begin; i!=words_end; i++) {
	    	if (endsWith(i->str(), ".png")) {
	    		std::cout << "Image png : ";
	    	}
	    	std::cout << i->str() << std::endl;
	    }
		*/

	    std::ofstream myfile;
		myfile.open ("test.jpg", std::fstream::binary);
		std::cout << "writing " << html.size() << " octets to a file" << std::endl;
		myfile.write(html.data(), html.size());
		myfile.close();

	    //std::cout << html << std::endl;

	} catch (const std::exception &e) {
		std::cout << "An error occured : " << e.what() << std::endl;
	}
	std::cout << "----------------------------" << std::endl;
}

int main(int argc, char ** argv) {

	if (argc < 2) {
		std::cout << "Usage : " << argv[0] << " URL" << std::endl;
		return 1;
	}

	test(argv[1]);

	/*
	test("http://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Transfer-Encoding");
	test("http://www.google.com");
	test("https://www.google.com");
	test("www.google.com:8080");
	test("http://www.google.com:8080");
	test("https://www.google.com:8080");
	test("www.google.com/test/test.html?action=test&var=value");
	test("http://www.google.com/test/test.html?action=test&var=value");
	test("https://www.google.com/test/test.html?action=test&var=value");
	test("www.google.com:8080/test/test.html?action=test&var=value");
	test("http://www.google.com:8080/test/test.html?action=test&var=value");
	test("https://www.google.com:8080/test/test.html?action=test&var=value");
	*/

	return 0;
}